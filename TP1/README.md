# Questions Atelier 1

## Qu’est-ce que le pattern MVC ? 

Le pattern MVC veut dire "Model", "Vue", "Controller". Il permet de séparer les responsabilités, la vue ne s'occupe que du rendu visuel d'une page html, le model représente les données et enfin le controller traite les requêtes du client, il demande les données du model pour les fournir à la vue.

## Quels avantages présente-t-il ? 

Une séparation des responsabilités, un code plus propre car on ne mélange pas la vue, la gestion de la requêtes, la logic métier et les intéractions à la BDD dans le même fichier (looking at you php)

## Qu’est-ce que le Web dynamique ?


## pourquoi est-il intéressant ?


## Comment sont récupérés les fichiers par le Web Browser en Web statique ?


## Quels sont les avantages d’utiliser du Web Statique avec des services REST ?


## Comment fonction l’AJAX ?


## Qu’est-ce que JEE ?


## Comment fonctionne un serveur JEE ?


## Qu’est-ce qu’un Web Container en JEE ?


## Que représente les Servlet dans JEE ?


## Qu’est-ce que JSP ?


## Quel est le cycle de vie d’un JSP ?


## Qu’est-ce que les expressions EL ?


## Que permettent de faire les expressions EL ? 


## Qu’apporte JSTL aux JSP ?


## Qu’est-ce qu’un Javabean ?


## Quels sont ces propriétés/contraintes ?


## Comment utilise-t-on les Javabean avec les JSP ?


## Que permet de faire JDBC ? 

Java Databsse Connector permet d’invoquer des requêtes SQL depuis un programme Java

## Quelle est la différence entre Statement et PrepareStatement ?

Le prepared statement permet de stocker la requête compilé dans l'instance et donc plus optomisé lors d'appels successifs.

Le mode d'exécution est identique mais le PreparedStatement permet de faire des requètes paramétrées sans risque d'injection SQL.

