$("#form").submit(function (event) {
    event.preventDefault();
    let endpoint = "http://localhost:8080/rest/servicescard/add";
    let card = {
        name: $('[name="name"]').val(),
        description: $('[name="description"]').val(),
        family: $('[name="namfamilye"]').val(),
        hp: $('[name="hp"]').val(),
        energy: $('[name="energy"]').val(),
        defence: $('[name="defence"]').val(),
        attack: $('[name="attack"]').val(),
        imgUrl: $('[name="imgUrl"]').val()
    }
    $.ajax({
        url: endpoint,
        type: "POST",
        data: JSON.stringify(card),
        contentType: 'application/json; charset=utf-8',
        dataType: "json"
    });
});