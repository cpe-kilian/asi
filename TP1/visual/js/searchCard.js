$(() => {
    let endpoint = "http://localhost:8080/rest/servicescard/cards";
    $.get(endpoint, function(data) {
        let myData = data[0];
        $('#cardFamilyImgId')[0].src="https://upload.wikimedia.org/wikipedia/commons/thumb/1/1c/DC_Comics_logo.png/280px-DC_Comics_logo.png";
        $('#cardFamilyNameId')[0].innerText=myData.family;
        $('#cardImgId')[0].src=myData.imgUrl;
        $('#cardNameId')[0].innerText=myData.name;
        $('#cardDescriptionId')[0].innerText=myData.description;
        $('#cardHPId')[0].innerText=myData.hp + " HP";
        $('#cardEnergyId')[0].innerText=myData.energy + " Energy";
        $('#cardAttackId')[0].innerText=myData.attack + " Attack";
        $('#cardDefenceId')[0].innerText=myData.defence + " Defence";
    });
})

function submit() {
    let endpoint = "http://localhost:8080/rest/servicescard/find";
    let name = $('[name="search"]').val();
    $.get(endpoint + "?name=" + name, function (data) {
        if (data != null) {
            $('#cardFamilyImgId')[0].src="https://upload.wikimedia.org/wikipedia/commons/thumb/1/1c/DC_Comics_logo.png/280px-DC_Comics_logo.png";
            $('#cardFamilyNameId')[0].innerText=data.family;
            $('#cardImgId')[0].src=data.imgUrl;
            $('#cardNameId')[0].innerText=data.name;
            $('#cardDescriptionId')[0].innerText=data.description;
            $('#cardHPId')[0].innerText=data.hp + " HP";
            $('#cardEnergyId')[0].innerText=data.energy + " Energy";
            $('#cardAttackId')[0].innerText=data.attack + " Attack";
            $('#cardDefenceId')[0].innerText=data.defence + " Defence";
        }
    })
}


