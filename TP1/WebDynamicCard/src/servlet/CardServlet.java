package servlet;

import controler.CardDao;
import model.CardModel;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/card")
public class CardServlet extends HttpServlet {
    private static final String NCARD = "card";
    private static final String NDAO = "DAO";
    private static final long serialVersionUID = 1L;
    private CardDao dao;

    public CardServlet() {
        super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        getDao();
        CardModel card = this.dao.getCard(request.getParameter("id"));
        request.setAttribute(NCARD, card);
        this.getServletContext().getRequestDispatcher( "/WEB-INF/card.jsp" ).forward( request, response );
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    public void getDao(){
        if(this.getServletContext().getAttribute(NDAO)!=null){
            this.dao=(CardDao) this.getServletContext().getAttribute(NDAO);
        }else{
            this.dao=new CardDao();
            this.getServletContext().setAttribute(NDAO,this.dao);
        }
    }
}
