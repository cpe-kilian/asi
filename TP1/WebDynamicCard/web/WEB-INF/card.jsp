<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Card ${requestScope.nponey.name}</title>
</head>
    <body>
        <h1>{requestScope.card.name}</h1>
        <p>Description: ${requestScope.card.description}</p>
        <p>Family: ${requestScope.card.family}</p>
        <p>HP: ${requestScope.card.hp}</p>
        <p>Energy: ${requestScope.card.energy}</p>
        <p>Defence: ${requestScope.card.defence}</p>
        <p>Attack: ${requestScope.card.attack}</p>
        <img src=${requestScope.card.imgUrl} >
    </body>
</html>
