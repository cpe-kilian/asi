--
-- PostgreSQL database dump
--

-- Dumped from database version 10.1
-- Dumped by pg_dump version 11.2

-- Started on 2020-04-08 18:50:23

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 226 (class 1259 OID 16736)
-- Name: Card; Type: TABLE; Schema: public; Owner: tp
--

CREATE TABLE public."Card" (
    id integer NOT NULL,
    name character varying(150),
    description text,
    family character varying(150),
    hp integer,
    energy integer,
    attack integer,
    defence integer,
    "imgUrl" text
);


--ALTER TABLE public."Card" OWNER TO tp;

--
-- TOC entry 225 (class 1259 OID 16734)
-- Name: Card_id_seq; Type: SEQUENCE; Schema: public; Owner: tp
--

CREATE SEQUENCE public."Card_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--ALTER TABLE public."Card_id_seq" OWNER TO tp;

--
-- TOC entry 2881 (class 0 OID 0)
-- Dependencies: 225
-- Name: Card_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: tp
--

ALTER SEQUENCE public."Card_id_seq" OWNED BY public."Card".id;


--
-- TOC entry 2750 (class 2604 OID 16739)
-- Name: Card id; Type: DEFAULT; Schema: public; Owner: tp
--

ALTER TABLE ONLY public."Card" ALTER COLUMN id SET DEFAULT nextval('public."Card_id_seq"'::regclass);


--
-- TOC entry 2875 (class 0 OID 16736)
-- Dependencies: 226
-- Data for Name: Card; Type: TABLE DATA; Schema: public; Owner: tp
--

COPY public."Card" (id, name, description, family, hp, energy, attack, defence, "imgUrl") FROM stdin;
3	DeadPool	Le passé exact de Deadpool reste flou, plusieurs versions de son passé s\\'étant succédé.\\n\\nConnu sous le nom de Wade Wilson, il perd son père à l\\'âge de 5 ans. Une autre version veut que son père l\\'ait abandonné jeune. La plupart des versions relatent une enfance difficile. Cependant, la version la plus récente veut que ses parents aient toujours été en vie et qu\\'il ait eu une enfance normale.\\n\\nDès ses 19 ans, il travaille en tant qu\\'assassin pour une cellule secrète de la CIA, n\\'acceptant que les missions auxquelles il croit, sans désir d’amasser une quelconque somme d\\'argent. Ne voyant que très rarement sa femme, il décide de la quitter lorsqu\\'il apprend qu\\'il a un cancer, ne voulant pas la voir souffrir. Se sachant condamné par de multiples tumeurs, il accepte de servir de cobaye au Département K Canadien dans le cadre du projet Arme X. L\\'expérience échoue et, considéré comme un rebut, il sert de cobaye au professeur Killbrew qui, par jeu et par plaisir sadique, le dote du même pouvoir d\\'auto-guérison (facteur auto-guérisseur) que le mutant Wolverine, un de ses principaux ennemis qui, plus tard, deviendra son ami. L\\'expérience réussit mais détruit sa peau, qui prend un aspect rongé et craquelé. Pendant cette période, les expériences de mort imminente qu\\'il vit lui permettent de rencontrer la Mort elle-même, dont il tombe amoureux. Dans cet institut, son seul ami est tué par Ajax, un assistant de Killbrew, après que Deadpool se fut moqué de son nom, « Francis ». Ajax arrache alors le cœur de Deadpool pour le tuer, mais celui-ci, dans un instinct de vengeance, le fait repousser et part se venger.\\n\\nIl prend alors le nom de Deadpool (littéralement, la « cagnotte du mort » en français), en référence au jeu pratiqué par les gardiens du Département K qui misaient sur le prochain patient à mourir, Wilson se présentant alors ironiquement comme « le roi de la Deadpool ». Après son évasion du Département K, il reprend son activité passée de mercenaire. '	MARVEL	100	100	10	50	http://fairedesgifs.free.fr/da/sh/deadpool/dp%20(18).gif
4	Hulk	Le docteur Bruce Banner, un brillant physicien nucléaire, crée pour les forces armées des États-Unis un nouveau type d\\'arme nucléaire, la bombe G, basée sur des rayons gamma.\\n\\nDurant un essai, Banner aperçoit un adolescent, Rick Jones, allongé dans sa voiture, qui répond bêtement à un pari de ses camarades. Demandant à son assistant Igor Starsky de stopper le compte à rebours, le physicien court vers la zone d\\'essai et sauve le jeune inconscient en le poussant dans une tranchée de protection. Mais Starsky, qui se nomme en réalité Drenkov, est un espion russe à la solde du gouvernement envoyé pour s\\'emparer des secrets de Banner. Il laisse la bombe exploser. Le docteur n\\'a pas le temps de se mettre à l\\'abri ; il est alors bombardé de rayons gamma, ce qui a pour effet de modifier profondément son ADN, mais qui, à la surprise de tous, ne le tue pas12,13.\\n\\nBanner reste en observation à l\\'infirmerie en compagnie de Rick Jones jusqu\\'au soir où Banner, dans une intense souffrance, voit son corps se transformer en un individu aux proportions colossales tandis que sa peau devient grise et que son psychisme se modifie pour adopter une intelligence quasi animale. Dans cet état, il détruit le mur de l\\'infirmerie simplement en le poussant de la main puis, lorsqu\\'une jeep de l\\'armée lui fonce dessus, reste immobile alors que le véhicule s\\'écrase sur lui sans qu\\'il subisse de dommages, démontrant ainsi posséder une force et une résistance stupéfiante. En raison de sa corpulence, un garde le surnomme « Hulk »12 lors d\\'une description faite à ses supérieursa.\\n\\nDepuis, lors de moments de stress ou de colère intense, le docteur Banner se métamorphose en une créature colossale à la peau verte (cette couleur ayant rapidement remplacé la peau grise originale) dotée d\\'une force phénoménale et animée par une rage qu\\'il ne parvient pas à contrôler (cette rage est présente en Banner depuis sa jeunesse quand, petit garçon, il est battu par son père et quand il vit sa mère mourir sous les coups de ce dernier). 	MARVEL	200	100	50	5	http://fairedesgifs.free.fr/da/sh/hulk/hulk%20(3).gif
1	superman	super	DC	100	100	50	50	http://fairedesgifs.free.fr/da/sh/superman/superman%20(5).gif
2	batman	super	DC	50	50	100	20	http://fairedesgifs.free.fr/da/sh/batman/batman%20(24).gif
\.


--
-- TOC entry 2882 (class 0 OID 0)
-- Dependencies: 225
-- Name: Card_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tp
--

SELECT pg_catalog.setval('public."Card_id_seq"', 1, false);


--
-- TOC entry 2752 (class 2606 OID 16744)
-- Name: Card Card_pkey; Type: CONSTRAINT; Schema: public; Owner: tp
--

ALTER TABLE ONLY public."Card"
    ADD CONSTRAINT "Card_pkey" PRIMARY KEY (id);


-- Completed on 2020-04-08 18:50:24

--
-- PostgreSQL database dump complete
--

