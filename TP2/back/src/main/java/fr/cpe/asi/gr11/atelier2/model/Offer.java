package fr.cpe.asi.gr11.atelier2.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class Offer {
    @Id
    @GeneratedValue(generator = "offer_sequence")
    private int id;
    @OneToOne
    private Card card;
    private int price;

    public Offer() {
    }

    public Offer(int id, Card card, int price) {
        this.id = id;
        this.card = card;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Card getCard() {
        return card;
    }

    public void setCard(Card card) {
        this.card = card;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
