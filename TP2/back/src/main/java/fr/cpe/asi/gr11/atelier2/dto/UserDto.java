package fr.cpe.asi.gr11.atelier2.dto;

public class UserDto {
    private String login;
    private String password;

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }
}
