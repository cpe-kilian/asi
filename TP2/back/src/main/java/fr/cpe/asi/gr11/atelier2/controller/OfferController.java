package fr.cpe.asi.gr11.atelier2.controller;

import fr.cpe.asi.gr11.atelier2.dto.OfferDto;
import fr.cpe.asi.gr11.atelier2.model.Card;
import fr.cpe.asi.gr11.atelier2.model.Offer;
import fr.cpe.asi.gr11.atelier2.model.User;
import fr.cpe.asi.gr11.atelier2.service.CardService;
import fr.cpe.asi.gr11.atelier2.service.OfferService;
import fr.cpe.asi.gr11.atelier2.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;

@CrossOrigin
@RestController
public class OfferController {
    @Autowired
    OfferService offerService;
    @Autowired
    CardService cardService;
    @Autowired
    UserService userService;

    @RequestMapping(method = RequestMethod.GET, value = "/offers")
    public Iterable<Offer> getOfferList() {
        return offerService.getOfferList();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/offers/{id}")
    public Offer getOffer(@PathVariable int id) {
        return offerService.getOffer(id);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/offers")
    public boolean sellCard(@RequestBody OfferDto o) {
        Card card = cardService.getCard(o.getCardId());
        if (!card.isForSale()) {
            card.setForSale(true);
            cardService.saveCard(card);
            offerService.addOffer(new Offer(0, card, o.getPrice()));
            return true;
        }
        return false;
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/offers/{id}")
    public boolean buyCard(@PathVariable int id, @RequestParam int userId) {
        Offer offer = offerService.getOffer(id);
        Card card = offer.getCard();
        User previousOwner = card.getOwner();
        User newOwner = userService.getUser(userId);
        if (newOwner.getCredits() >= offer.getPrice()) {
            previousOwner.setCredits(previousOwner.getCredits() + offer.getPrice());
            newOwner.setCredits(newOwner.getCredits() - offer.getPrice());
            card.setOwner(newOwner);
            card.setForSale(false);
            cardService.saveCard(card);
            userService.saveUsers(Arrays.asList(previousOwner, newOwner));
            offerService.removeOffer(offer);
            return true;
        }
        return false;
    }
}
