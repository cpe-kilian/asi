package fr.cpe.asi.gr11.atelier2.controller;

import fr.cpe.asi.gr11.atelier2.dto.CardDto;
import fr.cpe.asi.gr11.atelier2.model.Card;
import fr.cpe.asi.gr11.atelier2.service.CardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
public class CardController {
    @Autowired
    CardService cardService;

    @RequestMapping(method= RequestMethod.GET,value="/cards/{id}")
    public Card getCard(@PathVariable int id)
    {
        return cardService.getCard(id);
    }

    @RequestMapping(method= RequestMethod.GET,value="/cards/")
    public Card getCardTitle(@RequestParam String title)
    {
        return cardService.getCardTitle(title);
    }

    @RequestMapping(method= RequestMethod.GET,value="/cards")
    public Iterable<Card> getCardList()
    {
        return cardService.getCardList();
    }

    @RequestMapping(method= RequestMethod.POST,value="/cards")
    public void addCard(@RequestBody CardDto card)
    {
        Card card1 = new Card(card.getTitle(),card.getDescription(),card.getFamily(),card.getHp(),card.getEnergy(),
                card.getAttack(),card.getDefence(), card.getImageUrl());
        cardService.addCard(card1);
    }
}
