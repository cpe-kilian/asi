package fr.cpe.asi.gr11.atelier2.repository;

import fr.cpe.asi.gr11.atelier2.model.Card;
import fr.cpe.asi.gr11.atelier2.model.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface CardRepository extends CrudRepository<Card,Integer> {
    public Optional<Card> findByTitle(String title);
    public List<Card> findByOwnerIsNull();
}
