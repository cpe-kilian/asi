package fr.cpe.asi.gr11.atelier2.service;

import fr.cpe.asi.gr11.atelier2.model.Card;
import fr.cpe.asi.gr11.atelier2.model.User;
import fr.cpe.asi.gr11.atelier2.repository.CardRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

@Service
public class CardService {
    @Autowired
    CardRepository cardRepository;

    public void addCard(Card card){
        Card createdCard=cardRepository.save(card);
        System.out.println(createdCard);
    }

    public Card getCard(int id){
        Optional<Card> cardOptional =cardRepository.findById(id);
        if(cardOptional.isPresent()){
            return cardOptional.get();
        }else{
            return null;
        }
    }

    public Card getCardTitle(String title){
        Optional<Card> cardOptional =cardRepository.findByTitle(title);
        if(cardOptional.isPresent()){
            return cardOptional.get();
        }else{
            return null;
        }
    }

    public Iterable<Card> getCardList(){
        return cardRepository.findAll();
    }

    public void setRandomCardsForUser(User user) {
        List<Card> cards = cardRepository.findByOwnerIsNull();
        List<Card> userCards = new ArrayList<>();
        Random random = new Random();
        for (int i = 0; i < 5; i++) {
            if (cards.isEmpty()) break;
            int randomId = random.nextInt(cards.size());
            userCards.add(cards.remove(randomId));
        }
        userCards.forEach(card -> card.setOwner(user));
        cardRepository.saveAll(userCards);
    }

    public void saveCard(Card card) {
        cardRepository.save(card);
    }
}
