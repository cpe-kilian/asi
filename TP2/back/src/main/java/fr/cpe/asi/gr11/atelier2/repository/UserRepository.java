package fr.cpe.asi.gr11.atelier2.repository;

import fr.cpe.asi.gr11.atelier2.model.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UserRepository extends CrudRepository<User,Integer> {
    public List<User> findByLogin(String login);
}
