package fr.cpe.asi.gr11.atelier2.repository;

import fr.cpe.asi.gr11.atelier2.model.Offer;
import org.springframework.data.repository.CrudRepository;

public interface OfferRepository extends CrudRepository<Offer, Integer> {
}
