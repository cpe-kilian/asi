package fr.cpe.asi.gr11.atelier2.dto;

public class CardDto {
    private String title;
    private String description;
    private String family;
    private int hp;
    private int energy;
    private int attack;
    private int defence;
    private int price;
    private String imageUrl;

    public String getFamily() {
        return family;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public int getHp() {
        return hp;
    }

    public int getEnergy() {
        return energy;
    }

    public int getAttack() {
        return attack;
    }

    public int getDefence() {
        return defence;
    }

    public int getPrice() {
        return price;
    }

    public String getImageUrl() {
        return imageUrl;
    }
}
