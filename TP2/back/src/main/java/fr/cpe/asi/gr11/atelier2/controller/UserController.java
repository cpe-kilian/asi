package fr.cpe.asi.gr11.atelier2.controller;

import fr.cpe.asi.gr11.atelier2.dto.UserDto;
import fr.cpe.asi.gr11.atelier2.model.User;
import fr.cpe.asi.gr11.atelier2.service.CardService;
import fr.cpe.asi.gr11.atelier2.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
public class UserController {
    @Autowired
    UserService userService;
    @Autowired
    CardService cardService;

    @RequestMapping(method= RequestMethod.GET,value="/users/{id}")
    public User getUser(@PathVariable int id)
    {
        return userService.getUser(id);
    }

    @RequestMapping(method= RequestMethod.GET,value="/users")
    public Iterable<User> getUserList()
    {
        return userService.getUserList();
    }

    @RequestMapping(method= RequestMethod.POST,value="/register")
    public void register(@RequestBody UserDto u)
    {
        User user = new User(u.getLogin(), u.getPassword());
        user.setCredits(300);
        userService.addUser(user);
        cardService.setRandomCardsForUser(user);
    }

    @RequestMapping(method= RequestMethod.POST,value="/login")
    public User login(@RequestBody UserDto u)
    {
        User user = userService.findUser(u.getLogin());
        if (user.getPassword().equals(u.getPassword())) {
            return user;
        }
        return null;
    }
}
