var selectedId;
$(document ).ready(function(){
    getCards();
    //fillCurrentCard("https://upload.wikimedia.org/wikipedia/commons/thumb/1/1c/DC_Comics_logo.png/280px-DC_Comics_logo.png","DC comics","http://www.guinnessworldrecords.com/images/superlative/superheroes/GWR-Superheroes-SUPERMAN.svg","SUPERMAN","The origin story of Superman relates that he was born Kal-El on the planet Krypton, before being rocketed to Earth as an infant by his scientist father Jor-El, moments before Krypton's destruction. Discovered and adopted by a farm couple from Kansas, the child is raised as Clark Kent and imbued with a strong moral compass. Early in his childhood, he displays various superhuman abilities, which, upon reaching maturity, he resolves to use for the benefit of humanity through a 'Superman' identity.",50,100,17,8,100);

    $("#card").hide();
    $('#buy-button').click(buyCard);

    if(sessionStorage['login'])
    {
        $("#current-credits").text(sessionStorage['credits']);
        $("#userNameId").text(sessionStorage['login']);
    }else{
        window.location.href="./login.html";
    }
});




function fillCurrentCard(imgUrlFamily,familyName,imgUrl,name,description,hp,energy,attack,defence,price,id){
    //FILL THE CURRENT CARD
    $('#cardFamilyImgId')[0].src=imgUrlFamily;
    $('#cardFamilyNameId')[0].innerText=familyName;
    $('#cardImgId')[0].src=imgUrl;
    $('#cardNameId')[0].innerText=name;
    $('#cardDescriptionId')[0].innerText=description;
    $('#cardHPId')[0].innerText=hp+" HP";
    $('#cardEnergyId')[0].innerText=energy+" Energy";
    $('#cardAttackId')[0].innerText=attack+" Attack";
    $('#cardDefenceId')[0].innerText=defence+" Defence";
    $('#cardPriceId')[0].innerText=price +" $";
    $("#card").show();
    selectedId=id;
};


function addCardToList(imgUrlFamily,familyName,imgUrl,name,description,hp,energy,attack,defence,price,id){
    
    content="\
    <td> \
    <img  class='ui avatar image' src='"+imgUrl+"'> <span>"+name+" </span> \
   </td> \
    <td>"+description+"</td> \
    <td>"+familyName+"</td> \
    <td>"+hp+"</td> \
    <td>"+energy+"</td> \
    <td>"+attack+"</td> \
    <td>"+defence+"</td> \
    <td>"+price+"$</td>";

    $('#cardListId tr:last').after('<tr>'+content+'</tr>');

    $('#cardListId tr:last').click(function(){
        fillCurrentCard(imgUrlFamily,familyName,imgUrl,name,description,hp,energy,attack,defence,price,id);
    });


    
};

function getCards() {
    $.get("http://localhost:8080/offers", function(data) {
        data.forEach(element=>{
            console.log(element);
            if(element.card && element.price){
                addCardToList(element.card.imageUrl,element.card.family,element.card.imageUrl,element.card.title,element.card.description,element.card.hp,element.card.energy,element.card.attack,element.card.defence,element.price,element.id);
            }
        })
    })
}

function buyCard(){
    $.ajax({
        type: "DELETE",
        url: "http://localhost:8080/offers/"+selectedId+"?userId="+sessionStorage['id'],
        success: refreshUser
    });
}

function refreshUser(){
    $.ajax({
        type:"GET",
        url:"http://localhost:8080/users/"+sessionStorage['id'],
        success:handleBuyResponse
    })
}

function handleBuyResponse(data){
    sessionStorage['credits']=data.credits;
    window.location.reload();
}