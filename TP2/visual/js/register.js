$("#register-form").submit(function(event){
    event.preventDefault();
    console.log($("#password").val());
    console.log($("#re-password").val());
    if($("#password").val()==$("#re-password").val()){
        var data = {
            login: $("#login").val(),
            password : $("#password").val()
        };
        console.log(data);
        $.ajax({
            type: "POST",
            contentType: "application/json",
            url: "http://localhost:8080/register",
            data: JSON.stringify(data),
            success: handleRegisterResponse
        });
    }
})

function handleRegisterResponse(data){
    console.log(data);
    window.location.href="./login.html";
}