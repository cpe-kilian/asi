package fr.cpe.asi.gr11.atelier3.dto;

import org.hibernate.annotations.ColumnDefault;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

public class CardOfferDto {
    private int id;
    private String title;
    private String description;
    private String family;
    private int hp;
    private int energy;
    private int attack;
    private int defence;
    private String imageUrl;
    private boolean forSale;
    private int ownerId;

    public CardOfferDto() {
    }

    public CardOfferDto(String title, String description, String family, int hp, int energy, int attack, int defence, String imageUrl, int ownerId) {
        this.title = title;
        this.description = description;
        this.family = family;
        this.hp = hp;
        this.energy = energy;
        this.attack = attack;
        this.defence = defence;
        this.imageUrl = imageUrl;
        this.forSale = false;
        this.ownerId = ownerId;
    }

    public String getFamily() {
        return family;
    }

    public void setFamily(String family) {
        this.family = family;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public int getEnergy() {
        return energy;
    }

    public void setEnergy(int energy) {
        this.energy = energy;
    }

    public int getAttack() {
        return attack;
    }

    public void setAttack(int attack) {
        this.attack = attack;
    }

    public int getDefence() {
        return defence;
    }

    public void setDefence(int defence) {
        this.defence = defence;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public int getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(int ownerId) {
        this.ownerId = ownerId;
    }

    public boolean isForSale() {
        return forSale;
    }

    public void setForSale(boolean inOffer) {
        this.forSale = inOffer;
    }
}
