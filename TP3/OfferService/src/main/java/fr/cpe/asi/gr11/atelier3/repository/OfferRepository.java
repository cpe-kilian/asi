package fr.cpe.asi.gr11.atelier3.repository;

import fr.cpe.asi.gr11.atelier3.model.Offer;
import org.springframework.data.repository.CrudRepository;

public interface OfferRepository extends CrudRepository<Offer, Integer> {
}
