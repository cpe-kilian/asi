package fr.cpe.asi.gr11.atelier3.dto;

import javax.persistence.*;

@Entity
public class UserOfferDto {
    @Id
    @GeneratedValue(generator = "user_sequence")
    private int id;
    @Column(unique = true)
    private String login;
    private int credits;

    public UserOfferDto() {
    }

    public UserOfferDto(int id, String login, int credits) {
        this.id = id;
        this.login = login;
        this.credits = credits;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public int getCredits() {
        return credits;
    }

    public void setCredits(int credits) {
        this.credits = credits;
    }
}
