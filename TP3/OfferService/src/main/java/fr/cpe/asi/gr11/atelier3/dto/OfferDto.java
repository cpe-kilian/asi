package fr.cpe.asi.gr11.atelier3.dto;

public class OfferDto {
    private int cardId;
    private int price;

    public int getCardId() {
        return cardId;
    }

    public int getPrice() {
        return price;
    }
}
