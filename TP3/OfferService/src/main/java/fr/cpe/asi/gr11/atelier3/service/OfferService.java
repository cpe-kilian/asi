package fr.cpe.asi.gr11.atelier3.service;

import fr.cpe.asi.gr11.atelier3.model.Offer;
import fr.cpe.asi.gr11.atelier3.repository.OfferRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OfferService {
    @Autowired
    OfferRepository offerRepository;

    public Iterable<Offer> getOfferList() {
        return offerRepository.findAll();
    }

    public Offer getOffer(int id) {
        return offerRepository.findById(id).orElse(null);
    }

    public void addOffer(Offer offer) {
        offerRepository.save(offer);
    }

    public void removeOffer(Offer offer) {
        offerRepository.delete(offer);
    }
}
