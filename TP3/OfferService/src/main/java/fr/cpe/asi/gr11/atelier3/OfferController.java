package fr.cpe.asi.gr11.atelier3;

import fr.cpe.asi.gr11.atelier3.dto.CardOfferDto;
import fr.cpe.asi.gr11.atelier3.dto.OfferDto;
import fr.cpe.asi.gr11.atelier3.dto.UserOfferDto;
import fr.cpe.asi.gr11.atelier3.model.Offer;
import fr.cpe.asi.gr11.atelier3.service.OfferService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

@RestController
public class OfferController {
    String userServiceUrl = "http://gateway/api/users";
    String cardServiceUrl = "http://gateway/api/cards";
    @Autowired
    OfferService offerService;
    @Autowired
    RestTemplate restTemplate;

    @RequestMapping(method = RequestMethod.GET, value = "/offers")
    public Iterable<Offer> getOfferList() {
        return offerService.getOfferList();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/offers/{id}")
    public Offer getOffer(@PathVariable int id) {
        return offerService.getOffer(id);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/offers")
    public ResponseEntity<?> sellCard(@RequestBody OfferDto o) {
        ResponseEntity<CardOfferDto> cardResponse = restTemplate.getForEntity(cardServiceUrl+"/"+o.getCardId(), CardOfferDto.class);
        if(cardResponse.getBody() == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        CardOfferDto card = cardResponse.getBody();

        if (!card.isForSale()) {
            card.setForSale(true);
            restTemplate.put(cardServiceUrl, card);
            offerService.addOffer(new Offer(0, card.getId(), o.getPrice()));
            return new ResponseEntity<>(HttpStatus.OK);
        }
        else {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/offers/{id}")
    public ResponseEntity<?> buyCard(@PathVariable int id, @RequestParam int userId) {
        Offer offer = offerService.getOffer(id);

        // Get card from Card Service
        ResponseEntity<CardOfferDto> cardResponse = restTemplate.getForEntity(cardServiceUrl+"/"+offer.getCardId(), CardOfferDto.class);
        if(cardResponse.getBody() == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        CardOfferDto card = cardResponse.getBody();

        // Get previous owner from user service
        ResponseEntity<UserOfferDto> previousOwnerResponse = restTemplate.getForEntity(userServiceUrl+"/"+card.getOwnerId(), UserOfferDto.class );
        if(cardResponse.getBody() == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        UserOfferDto previousOwner = previousOwnerResponse.getBody();

        // Get new owner from user service
        ResponseEntity<UserOfferDto> newOwnerResponse = restTemplate.getForEntity(userServiceUrl+"/"+userId, UserOfferDto.class);
        if(cardResponse.getBody() == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        UserOfferDto newOwner = newOwnerResponse.getBody();

        // Proceed transaction
        if (newOwner.getCredits() >= offer.getPrice()) {
            previousOwner.setCredits(previousOwner.getCredits() + offer.getPrice());
            newOwner.setCredits(newOwner.getCredits() - offer.getPrice());
            card.setOwnerId(newOwner.getId());
            card.setForSale(false);

            // Updating other services
            restTemplate.put(cardServiceUrl, card);
            restTemplate.put(userServiceUrl, previousOwner);
            restTemplate.put(cardServiceUrl, newOwner);
            offerService.removeOffer(offer);
            return new ResponseEntity<>(HttpStatus.OK);
        }
        else {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
    }
}
