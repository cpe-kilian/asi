package fr.cpe.asi.gr11.atelier3.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class Offer {
    @Id
    @GeneratedValue(generator = "offer_sequence")
    private int id;
    private int cardId;
    private int price;

    public Offer() {
    }

    public Offer(int id, int cardId, int price) {
        this.id = id;
        this.cardId = cardId;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCardId() {
        return cardId;
    }

    public void setCardId(int cardId) {
        this.cardId = cardId;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
