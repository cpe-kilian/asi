package fr.cpe.asi.gr11.atelier3;

import fr.cpe.asi.gr11.atelier3.dto.CardDto;
import fr.cpe.asi.gr11.atelier3.dto.UserIdDto;
import fr.cpe.asi.gr11.atelier3.model.Card;
import fr.cpe.asi.gr11.atelier3.service.CardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Map;

@CrossOrigin
@RestController
public class CardController {
    @Autowired
    CardService cardService;

    @RequestMapping(method= RequestMethod.GET,value="/cards/{id}")
    public Card getCard(@PathVariable int id)
    {
        System.out.println("GET /cards/"+id);

        return cardService.getCard(id);
    }

    @RequestMapping(method= RequestMethod.GET,value="/cards")
    public Iterable<Card> getCardTitle(@RequestParam Map<String, String> reqParam)
    {
        System.out.println("GET /cards");

        String title = reqParam.get("title");
        String owner = reqParam.get("owner");
        String saleFilter = reqParam.get("salefilter");

        if(title != null) {
            ArrayList tempList = new ArrayList<Card>();
            tempList.add(cardService.getCardTitle(title));
            return tempList;
        }
        else if(owner != null) {
            int ownerId = Integer.parseInt(owner);
            return cardService.getCardListFromOwner(ownerId);
        }
        else if(saleFilter != null && saleFilter == "true") {
            return cardService.getCardForSale();
        }
        else {
            return cardService.getCardList();
        }
    }

    @RequestMapping(method= RequestMethod.POST,value="/cards")
    public Card addCard(@RequestBody CardDto card)
    {
        System.out.println("POST /cards");

        Card card1 = new Card(card.getTitle(),card.getDescription(),card.getFamily(),card.getHp(),card.getEnergy(),
                card.getAttack(),card.getDefence(), card.getImageUrl(), card.getOwnerId());
        Card newCard = cardService.saveCard(card1);
        return newCard;
    }

    @RequestMapping(method=RequestMethod.PUT, value="/cards/randomcards")
    public ResponseEntity<?> setRandomCardsForUser(@RequestBody UserIdDto userIdDto) {
        System.out.println("PUT /cards/randomcards");

        cardService.setRandomCardsForUser(userIdDto.getId());
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.PUT, value="/cards")
    public Card updateUser(@RequestBody Card user) {
        System.out.println("PUT /cards");
        
        return cardService.saveCard(user);
    }
}
