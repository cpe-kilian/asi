package fr.cpe.asi.gr11.atelier3.repository;

import fr.cpe.asi.gr11.atelier3.model.Card;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface CardRepository extends CrudRepository<Card,Integer> {
    public Optional<Card> findByTitle(String title);
    public List<Card> findByOwnerId(Integer ownerId);
    public List<Card> findByForSaleTrue();
}
