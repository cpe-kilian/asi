package fr.cpe.asi.gr11.atelier3.service;

import fr.cpe.asi.gr11.atelier3.model.Card;
import fr.cpe.asi.gr11.atelier3.repository.CardRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

@Service
public class CardService {
    @Autowired
    CardRepository cardRepository;

    public Card saveCard(Card card){
        Card createdCard=cardRepository.save(card);
        System.out.println(createdCard);
        return createdCard;
    }

    public Card getCard(int id){
        Optional<Card> cardOptional =cardRepository.findById(id);
        if(cardOptional.isPresent()){
            return cardOptional.get();
        }else{
            return null;
        }
    }

    public Card getCardTitle(String title){
        Optional<Card> cardOptional =cardRepository.findByTitle(title);
        if(cardOptional.isPresent()){
            return cardOptional.get();
        }else{
            return null;
        }
    }

    public Iterable<Card> getCardList(){
        return cardRepository.findAll();
    }

    public Iterable<Card> getCardListFromOwner(int ownerId){
        return cardRepository.findByOwnerId(ownerId);
    }

    public Iterable<Card> getCardForSale() {
        return cardRepository.findByForSaleTrue();
    }

    public void setRandomCardsForUser(int userId) {
        List<Card> cards = cardRepository.findByOwnerId(0);
        System.out.println("null cards length : " + cards.size());
        List<Card> userCards = new ArrayList<>();
        Random random = new Random();
        for (int i = 0; i < 5; i++) {
            if (cards.isEmpty()) break;
            int randomId = random.nextInt(cards.size());
            userCards.add(cards.remove(randomId));
        }
        userCards.forEach(card -> card.setOwnerId(userId));
        cardRepository.saveAll(userCards);
    }
}
