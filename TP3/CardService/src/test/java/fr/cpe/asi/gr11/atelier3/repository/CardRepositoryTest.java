package fr.cpe.asi.gr11.atelier3.repository;

import static org.junit.Assert.assertTrue;

import fr.cpe.asi.gr11.atelier3.model.Card;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@DataJpaTest
public class CardRepositoryTest {

    @Autowired
    CardRepository cardRepository;

    @Before
    public void setUp(){cardRepository.save(new Card("titre","description","family",10,11,12,13,"imageUrl",1));}

    @After
    public void cleanUp(){cardRepository.deleteAll();}

    @Test
    public void saveCard(){
        cardRepository.save(new Card("card1","description","family",10,11,12,13,"imageUrl",1));
        assertTrue(true);
    }

    @Test
    public void saveAndGetUser() {
        cardRepository.deleteAll();
        cardRepository.save(new Card("card1","description","family",10,11,12,13,"imageUrl",1));
        List<Card> cardList = new ArrayList<>();
        cardRepository.findAll().forEach(cardList::add);
        assertTrue(cardList.size() == 1);
        assertTrue(cardList.get(0).getTitle().equals("card1"));
    }

    @Test
    public void findByOwnerId() {
        cardRepository.save(new Card("card1","description","family",10,11,12,13,"imageUrl",1));
        cardRepository.save(new Card("card2","description","family",10,11,12,13,"imageUrl",2));
        cardRepository.save(new Card("card3","description","family",10,11,12,13,"imageUrl",3));
        cardRepository.save(new Card("card4","description","family",10,11,12,13,"imageUrl",4));
        cardRepository.save(new Card("card5","description","family",10,11,12,13,"imageUrl",5));
        List<Card> cardList = new ArrayList<>();
        cardRepository.findByOwnerId(2).forEach(cardList::add);
        assertTrue(cardList.size() == 1);
    }

    @Test
    public void findByTitle() {
        cardRepository.save(new Card("card1","description","family",10,11,12,13,"imageUrl",1));
        cardRepository.save(new Card("card2","description","family",10,11,12,13,"imageUrl",2));
        cardRepository.save(new Card("card3","description","family",10,11,12,13,"imageUrl",3));
        cardRepository.save(new Card("card4","description","family",10,11,12,13,"imageUrl",4));
        cardRepository.save(new Card("card5","description","family",10,11,12,13,"imageUrl",5));
        List<Card> cardList = new ArrayList<>();
        cardList.add(cardRepository.findByTitle("card4").get());
        assertTrue(cardList.size() == 1);
    }
}
