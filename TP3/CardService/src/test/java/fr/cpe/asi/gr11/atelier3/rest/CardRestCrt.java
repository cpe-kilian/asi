package fr.cpe.asi.gr11.atelier3.rest;

import fr.cpe.asi.gr11.atelier3.model.Card;
import fr.cpe.asi.gr11.atelier3.service.CardService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

@RunWith(SpringRunner.class)
@WebMvcTest(value = CardRestCrt.class)
public class CardRestCrt {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CardService cService;

    Card mockCard = new Card("titre","description","family",10,11,12,13,"imageUrl",1);

    @Test
    public void retrieveCard() throws Exception{
        Mockito.when(
                cService.getCard(Mockito.anyInt())
        ).thenReturn(mockCard);

        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/cards/1").accept(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

        System.out.println(result.getResponse().getContentAsString());
        String expectedResult="{\"id\":1,\"title\":\"titre\",\"description\":\"description\",\"family\":\"family\",\"hp\":10,\"energy\":11,\"attack\":12,\"defence\":13,\"ownerId\":1}";

        JSONAssert.assertEquals(expectedResult, result.getResponse()
                .getContentAsString(), false);
    }
}
