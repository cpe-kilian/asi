package fr.cpe.asi.gr11.atelier3.model;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CardTest {
    @Test
    public void createCard(){
        Card card1 = new Card("titre","description","family",10,11,12,13,"imageUrl",1);
        assertEquals("titre", card1.getTitle());
        assertEquals("description", card1.getDescription());
        assertEquals("family", card1.getFamily());
        assertEquals(10, card1.getHp());
        assertEquals(11, card1.getEnergy());
        assertEquals(12, card1.getAttack());
        assertEquals(13, card1.getDefence());
        assertEquals("imageUrl", card1.getImageUrl());
        assertEquals(1, card1.getOwnerId());
        assertEquals(false, card1.isForSale());
    }
}
