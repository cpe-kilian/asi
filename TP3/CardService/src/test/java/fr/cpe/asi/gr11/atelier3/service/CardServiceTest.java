package fr.cpe.asi.gr11.atelier3.service;

import static org.junit.Assert.assertTrue;

import fr.cpe.asi.gr11.atelier3.model.Card;
import fr.cpe.asi.gr11.atelier3.repository.CardRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

@RunWith(SpringRunner.class)
@WebMvcTest(value = CardService.class)
public class CardServiceTest {

    @Autowired
    private CardService cService;

    @MockBean
    private CardRepository cRepo;

    Card tmpCard=new Card("titre","description","family",10,11,12,13,"imageUrl",1);

    @Test
    public void getCard(){
        Mockito.when(
                cRepo.findById(Mockito.any())
        ).thenReturn(Optional.ofNullable(tmpCard));
        Card cardInfo=cService.getCard(45);
        assertTrue(cardInfo.toString().equals(tmpCard.toString()));
    }
}
