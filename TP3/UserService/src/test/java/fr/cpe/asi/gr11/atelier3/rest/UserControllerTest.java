package fr.cpe.asi.gr11.atelier3.rest;

import fr.cpe.asi.gr11.atelier3.UserController;
import fr.cpe.asi.gr11.atelier3.model.User;
import fr.cpe.asi.gr11.atelier3.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

@RunWith(SpringRunner.class)
@WebMvcTest(value = UserController.class)
public class UserControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserService userService;

    User mockUser = new User(1, "mockuser", 0);

    @Test
    public void retrieveUser() throws Exception {
        Mockito.when(
                userService.getUser(Mockito.anyInt())
        ).thenReturn(mockUser);


        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/users/1").accept(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

        System.out.println(result.getResponse().getContentAsString());
        String expectedResult="{\"id\":1,\"login\":\"mockuser\",\"credits\":0}";

        JSONAssert.assertEquals(expectedResult, result.getResponse()
                .getContentAsString(), false);
    }

}

