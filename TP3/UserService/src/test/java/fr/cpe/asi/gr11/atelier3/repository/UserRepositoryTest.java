package fr.cpe.asi.gr11.atelier3.repository;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import fr.cpe.asi.gr11.atelier3.model.User;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest
public class UserRepositoryTest {

    @Autowired
    UserRepository userRepository;

    @Before
    public void setUp() {
        userRepository.save(new User(1, "test", 9999));
    }

    @After
    public void cleanUp() {
        userRepository.deleteAll();
    }

    @Test
    public void saveUser() {
        userRepository.save(new User(10, "testuser", 999999));
        assertTrue(true);
    }

    @Test
    public void saveAndGetUser() {
        userRepository.deleteAll();
        userRepository.save(new User(2, "testuser", 1000));
        List<User> userList = new ArrayList<>();
        userRepository.findAll().forEach(userList::add);
        assertTrue(userList.size() == 1);
        assertTrue(userList.get(0).getLogin().equals("testuser"));
    }

    @Test
    public void findByLogin() {
        userRepository.save(new User(2, "test2", 0));
        userRepository.save(new User(3, "test3", 0));
        userRepository.save(new User(4, "test4", 0));
        userRepository.save(new User(5, "test5", 0));
        userRepository.save(new User(6, "test6", 0));
        List<User> userList = new ArrayList<>();
        userRepository.findByLogin("test5").forEach(userList::add);
        assertTrue(userList.size() == 1);
    }
}

