package fr.cpe.asi.gr11.atelier3.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

public class UserTest {
    @Test
    public void createUser() {
        User user1 = new User(1, "test", 1000);
        assertEquals(1, user1.getId());
        assertEquals("test", user1.getLogin());
        assertEquals(1000, user1.getCredits());

        User user2 = new User(2, "test2", 0);
        assertEquals(2, user2.getId());
        assertEquals("test2", user2.getLogin());
        assertEquals(0, user2.getCredits());
    }
}
