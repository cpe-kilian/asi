package fr.cpe.asi.gr11.atelier3.service;

import static org.junit.Assert.assertTrue;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import fr.cpe.asi.gr11.atelier3.model.User;
import fr.cpe.asi.gr11.atelier3.repository.UserRepository;

@RunWith(SpringRunner.class)
@WebMvcTest(value = UserService.class)
public class UserServiceTest {

    @Autowired
    private UserService userService;

    @MockBean
    private UserRepository userRepository;

    User tmpUser = new User(1, "testuser", 1000);

    @Test
    public void getUser() {
        Mockito.when(
                userRepository.findById(Mockito.any())
        ).thenReturn(Optional.ofNullable(tmpUser));

        User userInfo = userService.getUser(45);
        assertTrue(userInfo.toString().equals(tmpUser.toString()));
    }
}

