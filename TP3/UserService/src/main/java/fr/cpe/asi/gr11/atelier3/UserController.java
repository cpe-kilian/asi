package fr.cpe.asi.gr11.atelier3;

import fr.cpe.asi.gr11.atelier3.dto.UserDto;
import fr.cpe.asi.gr11.atelier3.dto.UserIdDto;
import fr.cpe.asi.gr11.atelier3.model.User;
import fr.cpe.asi.gr11.atelier3.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Map;

@CrossOrigin
@RestController
public class UserController {
    String cardServiceRandomUrl = "http://gateway/api/cards/randomcards";
    @Autowired
    UserService userService;

    @Autowired
    RestTemplate restTemplate;

    @RequestMapping(method= RequestMethod.GET, value="/users")
    public Iterable<User> getUser(@RequestParam Map<String, String> reqParam) {
        System.out.println("GET /users");

        String login = reqParam.get("login");

        if (login != null) {
            ArrayList tempList = new ArrayList<User>();
            tempList.add(userService.findUser(login));
            return tempList;
        } else {
            return userService.getUserList();
        }
    }

    @RequestMapping(method= RequestMethod.POST, value="/users")
    public User createUser(@RequestBody UserDto u) {
        System.out.println("POST /users");

        User user = new User(0, u.getLogin(), 1000);
        User newUser = userService.saveUser(user);

        // Request to card service : setting random cards for the new user id
        restTemplate.put(cardServiceRandomUrl, new UserIdDto(newUser.getId()));

        return newUser;
    }

    @RequestMapping(method= RequestMethod.GET,value="/users/{id}")
    public User getUserById(@PathVariable int id)
    {
        System.out.println("GET /users/"+id);
        return userService.getUser(id);
    }

    @RequestMapping(method = RequestMethod.PUT, value="/users")
    public User updateUser(@RequestBody User user) {
        System.out.println("PUT /users");

        return userService.saveUser(user);
    }
}
