package fr.cpe.asi.gr11.atelier3.service;

import fr.cpe.asi.gr11.atelier3.model.User;
import fr.cpe.asi.gr11.atelier3.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {
    @Autowired
    UserRepository userRepository;

    public User saveUser(User user){
        User createdUser=userRepository.save(user);
        System.out.println(createdUser);
        return createdUser;
    }

    public User getUser(int id){
        Optional<User> userOptional =userRepository.findById(id);
        if(userOptional.isPresent()) {
            return userOptional.get();
        }else{
            return null;
        }
    }

    public User findUser(String login){
        List<User> user = userRepository.findByLogin(login);
        if(user.size()==1){
            return user.get(0);
        }else{
            return null;
        }
    }

    public Iterable<User> getUserList(){
        return userRepository.findAll();
    }

    public void saveUsers(List<User> users) {
        userRepository.saveAll(users);
    }
}
