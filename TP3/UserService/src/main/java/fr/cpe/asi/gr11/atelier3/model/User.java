package fr.cpe.asi.gr11.atelier3.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "user_table")
public class User {
    @Id
    @GeneratedValue(generator = "user_sequence")
    private int id;
    @Column(unique = true)
    private String login;
    private int credits;

    public User() {
    }

    public User(int id, String login, int credits) {
        this.id = id;
        this.login = login;
        this.credits = credits;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public int getCredits() {
        return credits;
    }

    public void setCredits(int credits) {
        this.credits = credits;
    }
}
