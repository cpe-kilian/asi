package fr.cpe.asi.gr11.atelier3.dto;

public class UserIdDto {
    private int id;

    public UserIdDto(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
}

