$("#login-form").submit(function(event){
    event.preventDefault();
    var data = {
        login: $("#login").val(),
        password : $("#password").val()
    };
    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "http://localhost/api/auth/login",
        data: JSON.stringify(data),
        success: handleLoginResponse
    });

})

function handleLoginResponse(data){
    console.log(data);
    sessionStorage['login'] = data.login;
    sessionStorage['id'] = data.id;
    sessionStorage['credits'] = data.credits;
    window.location.href="./";
}