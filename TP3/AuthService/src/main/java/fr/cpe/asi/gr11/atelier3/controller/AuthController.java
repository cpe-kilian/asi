package fr.cpe.asi.gr11.atelier3.controller;

import fr.cpe.asi.gr11.atelier3.dto.AuthDto;
import fr.cpe.asi.gr11.atelier3.dto.UserLoginDto;
import fr.cpe.asi.gr11.atelier3.dto.UserRegisterDto;
import fr.cpe.asi.gr11.atelier3.model.Auth;
import fr.cpe.asi.gr11.atelier3.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
public class AuthController {
    String userServiceUrl = "http://gateway/api/users";
    @Autowired
    AuthService authService;
    @Autowired
    RestTemplate restTemplate;

    @RequestMapping(method= RequestMethod.POST,value="/register")
    public ResponseEntity<?> register(@RequestBody AuthDto authDto) {
        System.out.println("POST /register");
        ResponseEntity<UserLoginDto> user = restTemplate.postForEntity(userServiceUrl, new UserRegisterDto(authDto.getLogin()), UserLoginDto.class);
        if (user.getBody() != null) {
            authService.saveAuth(new Auth(user.getBody().getId(), authDto.getPassword()));
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @RequestMapping(method= RequestMethod.POST,value="/login")
    public ResponseEntity<?> login(@RequestBody AuthDto authDto) {
        System.out.println("POST /login");
        ResponseEntity<UserLoginDto[]> user = restTemplate.getForEntity(userServiceUrl + "?login=" + authDto.getLogin(), UserLoginDto[].class);
        if (user.getBody() != null) {
            Auth auth = authService.getAuth(user.getBody()[0].getId());
            if (auth != null && authDto.getPassword().equals(auth.getPassword())) {
                System.out.println("success");
                return new ResponseEntity<>(user.getBody()[0], HttpStatus.OK);
            }
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
