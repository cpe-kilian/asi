package fr.cpe.asi.gr11.atelier3.service;

import fr.cpe.asi.gr11.atelier3.model.Auth;
import fr.cpe.asi.gr11.atelier3.repository.AuthRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AuthService {
    @Autowired
    AuthRepository authRepository;

    public Auth getAuth(int userId) {
        return authRepository.findById(userId).orElse(null);
    }

    public void saveAuth(Auth auth) {
        authRepository.save(auth);
    }
}
