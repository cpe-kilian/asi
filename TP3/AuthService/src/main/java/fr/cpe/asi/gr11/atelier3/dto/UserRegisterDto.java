package fr.cpe.asi.gr11.atelier3.dto;

public class UserRegisterDto {
    private String login;

    public UserRegisterDto(String login) {
        this.login = login;
    }


    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

}
