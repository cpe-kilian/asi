package fr.cpe.asi.gr11.atelier3.repository;

import fr.cpe.asi.gr11.atelier3.model.Auth;
import org.springframework.data.repository.CrudRepository;

public interface AuthRepository extends CrudRepository<Auth, Integer> {
}
