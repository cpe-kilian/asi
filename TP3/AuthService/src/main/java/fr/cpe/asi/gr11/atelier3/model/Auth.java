package fr.cpe.asi.gr11.atelier3.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Auth {
    @Id
    private int id;
    private String password;

    public Auth() {
    }

    public Auth(int id, String password) {
        this.id = id;
        this.password = password;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
